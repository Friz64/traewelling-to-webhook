import discord
from discord.ext import commands

import config
import db


class DiscordBot():
    def __init__(self, cfg: config.Config, database: db.DB):
        self.db = database

        intents = discord.Intents.default()
        intents.messages = True
        self.bot = commands.Bot(command_prefix=commands.when_mentioned, intents=intents)
        self.register_commands()

    def register_commands(self):
        @self.bot.command(name="addUsername")
        async def add_username(ctx, username: str):
            self.db.add_user(username)
            await ctx.send(f"Username {username} added")

        @self.bot.command(name="removeUsername")
        async def remove_username(ctx, username: str):
            self.db.remove_user(username)
            await ctx.send(f"Username {username} removed")