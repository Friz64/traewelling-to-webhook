import json

class Config():
    def __init__(self):
        self.config = {}

    def read_config(self, fp):
        f = open(fp, "r")
        self.config = json.load(f)

    def get_key(self, key):
        return self.config[key]